﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CursosDeProgramacion.Models
{
    public class Modulo
    {
        public int Id { get; set; }
        public string Titulo { get; set; }
        public string Descripcion { get; set; }
        public string Video { get; set; }
        public string Contenido { get; set; }
        public int CursoId { get; set; }
        public Curso Curso { get; set; }

    }
}
