﻿using System.Threading.Tasks;
using MailKit.Net.Smtp;
using MailKit;
using MimeKit;

namespace CursosDeProgramacion.Services
{
    public class EmailSettings
    {
        public string MailServer { get; set; }
        public int MailPort { get; set; }
        public string SenderName { get; set; }
        public string Sender { get; set; }
        public string Password { get; set; }
    }
    public interface IEmailSender
    {
        Task SendEmailAsync(string email, string subject, string message);
    }
    public class EmailSender: IEmailSender
    {
        private EmailSettings _settings;

        public EmailSender()
        {
            _settings = new EmailSettings
            {
                MailServer = "smtp.cursosdeprogramacion.net",
                MailPort = 465, 
                SenderName = "Cursosdeprogramacion.net",
                Sender = "no-reply@cursosdeprogramacion.net",
                Password = "FakePassword.1"
            };
        }

        public async Task SendEmailAsync(string email, string subject, string message)
        {
            var mail = new MimeMessage();
            mail.From.Add(new MailboxAddress(_settings.SenderName, _settings.Sender));
            mail.To.Add(new MailboxAddress(email, email));
            mail.Subject = subject;

            mail.Body = new TextPart("html")
            {
                Text = message
            };

            using (var client = new SmtpClient())
            {
                client.ServerCertificateValidationCallback = (s, c, h, e) => true;

                client.Connect(_settings.MailServer, _settings.MailPort, true);

                // Note: only needed if the SMTP server requires authentication
                await client.AuthenticateAsync(_settings.Sender, _settings.Password);

                await client.SendAsync(mail);
                await client.DisconnectAsync(true);
            }
        }
    }
}