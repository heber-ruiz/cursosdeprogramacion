﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;

namespace CursosDeProgramacion.Data
{
    public static class MyIdentityDataInitializer
    {
        public static void SeedData(UserManager<IdentityUser> userManager, RoleManager<IdentityRole> roleManager)
        {
            SeedRoles(roleManager);
            SeedUsers(userManager);
        }

        public static void SeedRoles(RoleManager<IdentityRole> roleManager)
        {
            if (!roleManager.RoleExistsAsync("NormalUser").Result)
            {
                roleManager.CreateAsync(new IdentityRole("NormalUser")).Wait();
            }

            if (!roleManager.RoleExistsAsync("AdministrativeUser").Result)
            {
                roleManager.CreateAsync(new IdentityRole("AdministrativeUser")).Wait();
            }
        }

        public static void SeedUsers(UserManager<IdentityUser> userManager)
        {
            if (userManager.FindByEmailAsync("admin@aprendeprogramacion.net").Result == null)
            {
                var user = new IdentityUser
                {
                    UserName = "admin@aprendeprogramacion.net",
                    Email = "admin@aprendeprogramacion.net"
                };

                userManager.CreateAsync(user, "Cursosdeprogramacion.1").Wait();
                userManager.AddToRoleAsync(user, "AdministrativeUser").Wait();
            }
        }
    }
}
